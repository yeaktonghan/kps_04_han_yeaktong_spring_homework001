package com.hrd.kps_04_han_yeaktong_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kps04HanYeaktongSpringHomework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Kps04HanYeaktongSpringHomework001Application.class, args);
    }

}
