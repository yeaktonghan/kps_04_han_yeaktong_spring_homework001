package com.hrd.kps_04_han_yeaktong_spring_homework001.controller;

import com.hrd.kps_04_han_yeaktong_spring_homework001.model.user.CustomerModel;
import com.hrd.kps_04_han_yeaktong_spring_homework001.response.CustomerResponseHandler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/")
public class Customer {
    static List<CustomerModel> listCustomer = new ArrayList<>();
    CustomerResponseHandler responseHandler;
    Date date = new Date();

    public static int idIndex = 3;

    Customer() {
//        listCustomer = new ArrayList<>();
        CustomerModel customerModel = new CustomerModel(1, "Demo User", "Male", 20, "Phnom Penh");
        CustomerModel customerModel2 = new CustomerModel(2, "Demo User 2", "Female", 23, "Takeo");
        listCustomer.add(customerModel);
        listCustomer.add(customerModel2);
    }

    @GetMapping("/customers")
    ResponseEntity<CustomerResponseHandler<List<CustomerModel>>> getCustomer() {
        try {
            responseHandler = new CustomerResponseHandler<List<CustomerModel>>();
            responseHandler.setMessage("Fetch Data Successfully");
            responseHandler.setData(listCustomer);
            responseHandler.setStatus("OK");
            responseHandler.setTime(date.toString());
            return ResponseEntity.ok().body(responseHandler);
        } catch (Exception e) {
            return null;
        }
//        return listCustomer;
    }

    @GetMapping("/customers/{customerId}")
    ResponseEntity<CustomerResponseHandler<List<CustomerModel>>> getCustomerById(@PathVariable int customerId) {
        try {
            // Loop all arraylist
            for (CustomerModel list : listCustomer) {
                // if found return data
                if (list.getId() == customerId) {
                    responseHandler = new CustomerResponseHandler<List<CustomerModel>>();
                    responseHandler.setMessage("Fetch Data Successfully");
                    responseHandler.setData(list);
                    responseHandler.setStatus("OK");
                    responseHandler.setTime(date.toString());
                    return ResponseEntity.ok().body(responseHandler);
                }
            }
            // if out of list element and not found return 404 not found. Exec when finish looping all list
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/customers/search")
    ResponseEntity<CustomerResponseHandler<List<CustomerModel>>> getCustomerById(@RequestParam String customerName) {
        try {
            // Loop all arraylist
            for (CustomerModel list : listCustomer) {
                // if found return data
                if (Objects.equals(list.getName(), customerName)) {
                    responseHandler = new CustomerResponseHandler<List<CustomerModel>>();
                    responseHandler.setMessage("Fetch Data Successfully");
                    responseHandler.setData(list);
                    responseHandler.setStatus("OK");
                    responseHandler.setTime(date.toString());
                    return ResponseEntity.ok().body(responseHandler);
                }
            }
            // if out of list element and not found return 404 not found. Exec when finish looping all list
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return null;
        }
    }

    @PostMapping(value = "/customers")
    ResponseEntity<CustomerResponseHandler<List<CustomerModel>>> addCustomer(@RequestBody CustomerModel customerModel) {
        try {
            listCustomer.add(new CustomerModel(idIndex, customerModel.getName(), customerModel.getGender(), customerModel.getAge(), customerModel.getAddress()));
            idIndex++;
            responseHandler = new CustomerResponseHandler<List<CustomerModel>>();
            responseHandler.setMessage("Fetch Data Successfully");
            responseHandler.setData(listCustomer.get(listCustomer.size() - 1));
            responseHandler.setStatus("OK");
            responseHandler.setTime(date.toString());
            return ResponseEntity.ok().body(responseHandler);
        } catch (Exception e) {
            return null;
        }
    }

    @DeleteMapping("/customers/{customerId}")
    ResponseEntity<CustomerResponseHandler<List<CustomerModel>>> deleteCustomerById(@PathVariable int customerId) {
        try {
            // Loop all arraylist
            for (CustomerModel list : listCustomer) {
                // if found return data
                if (list.getId() == customerId) {
                    listCustomer.remove(customerId - 1);
                    responseHandler = new CustomerResponseHandler<List<CustomerModel>>();
                    responseHandler.setMessage("Deleted Customer ID " + customerId + " Successfully");
                    responseHandler.setStatus("OK");
                    responseHandler.setTime(date.toString());
                    return ResponseEntity.ok().body(responseHandler);
                }
            }
            // if out of list element and not found return 404 not found. Exec when finish looping all list
            return ResponseEntity.notFound().build();

        } catch (Exception e) {
            return null;
        }
    }

    @PutMapping("/customers/{customerId}")
    ResponseEntity<CustomerResponseHandler<List<CustomerModel>>> updateCustomerById(@PathVariable int customerId, @RequestBody CustomerModel customerModel) {
        try {
            for (int i = 0; i < listCustomer.size(); i++) {
                if (listCustomer.get(i).getId() == customerId) {
                    listCustomer.get(i).setName(customerModel.getName());
                    listCustomer.get(i).setGender(customerModel.getGender());
                    listCustomer.get(i).setAge(customerModel.getAge());
                    listCustomer.get(i).setAddress(customerModel.getAddress());
                    responseHandler.setMessage("Customer " + customerId + " have been updated successfully");
                    responseHandler.setData(listCustomer.get(i));
                    responseHandler.setStatus("OK");
                    responseHandler.setTime(date.toString());
                    return ResponseEntity.ok().body(responseHandler);
                }
            }

            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return null;
        }
    }

}
